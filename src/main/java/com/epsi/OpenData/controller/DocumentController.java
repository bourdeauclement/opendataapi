package com.epsi.OpenData.controller;

import com.epsi.OpenData.Formatter.XmlFormatter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

@RestController
public class DocumentController {
	
	private static final String UPLOAD_FOLDER = "C:\\Users\\theog\\Documents\\EPSI\\MSPR_Maintenance\\test";
	
	/**
	 * Upload a document 
	 * @return state of uploaded file
	 */
	@PostMapping("/document")
	public @ResponseBody ResponseEntity<String> newDocument(
			@RequestParam("file") MultipartFile file) {
		
		if(file == null) {
			return new ResponseEntity<>("Invalid form data", HttpStatus.BAD_REQUEST);
		}
		
		String uploadedFileLocation = UPLOAD_FOLDER + "/" + file.getOriginalFilename();

		try {
			Path path = Paths.get(uploadedFileLocation);
			file.transferTo(path);
			Scanner sc = new Scanner(file.getInputStream());
			String result = "";
			while(sc.hasNext()) {
				result +=  sc.nextLine();
			}
			System.out.println(result);
			String xml = XmlFormatter.formatFromCSV(result);
			System.out.println(xml);
			return new ResponseEntity<>(xml, HttpStatus.OK);
		} catch (IOException e) {
			return new ResponseEntity<>("File not uploaded", HttpStatus.BAD_REQUEST);
		}
		

	}
}
