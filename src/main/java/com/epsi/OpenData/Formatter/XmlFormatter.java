package com.epsi.OpenData.Formatter;

public class XmlFormatter {
	
	/**
	 * Format a string to XML format
	 * @param unformattedString
	 * @return
	 */
	public static String formatFromCSV(String unformattedString) {
		String[] tab = unformattedString.split(";");
		String result = "<import>";
		for(int i=0; i < tab.length; i++) {
			int j = i % 3;
			switch(j) {
				case 0:
					result += "<data" + String.valueOf(i/3 + 1) + ">";
					result += "<uniqueID>" + tab[i] + "</uniqueID>";
					break;
				case 1:
					result += "<name>" + tab[i] + "</name>";
					break;
				case 2:
					result += "<value>" + tab[i] + "</value>";
					result += "</data" + String.valueOf(i/3 + 1) + ">";
					break;
			}
		}
		result += "</import>";
		System.out.println(result);
		return result;
	}
}
