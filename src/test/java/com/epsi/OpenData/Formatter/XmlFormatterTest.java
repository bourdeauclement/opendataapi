package com.epsi.OpenData.Formatter;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class XmlFormatterTest {

	@Test
	void testFormatFromCSV() {
		final String csv = "1;cle1;valeur1;" + 
				"2;Cle2;valeur2;" + 
				"3;cle3;valeur3";
		assertEquals("<import><data1><uniqueID>1</uniqueID><name>cle1</name><value>valeur1</value></data1><data2><uniqueID>2</uniqueID><name>Cle2</name><value>valeur2</value></data2><data3><uniqueID>3</uniqueID><name>cle3</name><value>valeur3</value></data3></import>",
				XmlFormatter.formatFromCSV(csv));
	}

}
